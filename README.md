# Commission calculator

Uses a shortcode to generate a range input that will calculate commission paid on a house sale. Commission is set as an option setting in the admin dashboard, as are the min/max input. Select whether to use a competitor's commission to display the difference between the two.

Tested up to WordPress 5.0.2, PHP 7.2.


## Shortcode usage

This shortcode can be used more thn once on a page.
This shortcode is not accessible through ID for styling purposes.
Classnames have been added to every element output.

### Notes

* Savings claculations are rounded although all currency and percentage items allow for two decimals.

#### Settings

The following settings appear on the settings page

1. `Lowest price` Default value one. The lowest price the house price input will accept.
2. `Highest price` Default value ten million. The highest price the house price input will accept.
3. `Price increments` Allows you to set the increments in house pricing to nearest 100, 1000, 10 000 and so on.
4. `Flat fee` Default value is zero. Should this be more than zero, the flat fee will be used in all calculations afainst competing commissions.
5. `Commission` Default value five. The percentage commission charged. Accepts two decimal places.
6. `Show comparison` Default value false. Should the input display the "savings made" against a competing commission.
7. `Competing commission` Default value eight. The commission to compare against. Accepts two decimal places.

#### Styling

`[sd_commission_calculator]`

Outputs

```html
<div class="sdcc_container">
    <input type="range" class="sdcc_range">
    <p class="sdcc_price_label">
        Your house price: <span class="sdcc_price"></span>
    </p>
    <p class="sdcc_diff_label">
        Saved on commission: <span class="sdcc_diff"></span>
    </p>
</div>
```
#### Output with title

`[sd_commission_calculator title="What you'll pay"]`

Outputs

```html
<div class="sdcc_container">
    <h3 class="sdcc_title">What you'll pay</h3>
    <input type="range" class="sdcc_range">
    <p class="sdcc_price_label">
        Your house price: <span class="sdcc_price"></span>
    </p>
    <p class="sdcc_diff_label">
        Saved on commission: <span class="sdcc_diff"></span>
    </p>
</div>
```

### Other shortcode parameters

All options can be over-ridden by shortcode parameters. The parameters are as follows.

* `title="""` Adds a title to the output (demonstrated above).
* `price_label` Defines the label for the house price. By default it is "Your house price:".
* `price_increments` Allows you to set the increments in house price. Use with care.
* `savings_label` Defines the label for the display of savings on commission. By default it is "Saved on commission:".
* `show_difference="true|false"` Over-rides the option set on the settings page. `false` suppresses output of the savings.
* `min` Overrides the option set on the settings page to define minimum house price input. Should be used as follows: `min="20"` or `min="200000"`
* `max` Overrides the option set on the settings page to define maximum house price input. Should be used as follows: `max="20"` or `max="200000"`
* `flat_fee` Overrides the flat fee setting on the plugin options page.

## Licence

GPL2

Copyright (c) 2018 Nic Wilson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.