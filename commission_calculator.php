<?php

/*
Plugin Name: SD Commission Calculator
Plugin URI: https://bitbucket.org/nicwilson/commission-calculator/overview
Description: A slider input of a value will return two results, the first a commission value (percentage of the input) and the second the difference between the first value and a second percentage of the input
Version: 1.0
Author: Nic Wilson
Author URI: https://solutiondesign.co.nz
License: GPL2
*/


namespace SolutionDesign\CommissionCalculator;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . '/includes/configuration.php' );
require_once( plugin_dir_path( __FILE__ ) . '/includes/shortcodes.php' );

if ( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . '/admin/plugin_setup.php' );
	require_once( plugin_dir_path( __FILE__ ) . '/includes/activate_deactivate.php' );
}


class commission_calculator {

	/**
	 * Version is stored in options table as nix_cc_version and checked on load
	 * There is an update function stub for use should you need it
	 */
	public $version = '1.0.0';

	protected static $instance = null;

	public static function init() {

		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __construct() {

		$version = get_option( 'sd_commission_calculator_version', false );

		if ( $version !== $this->version ) {

			self::plugin_updated();
		}

		if ( ! is_admin() ) {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );
		}

		$shortcodes = new shortcodes();
		add_shortcode( 'sd_commission_calculator', array( $shortcodes, 'commission_calculator' ) );

		add_action( 'admin_menu', array( 'SolutionDesign\CommissionCalculator\plugin_setup', 'init' ) );
	}

	/**
	 * load required scripts
	 */
	public function load_scripts() {

		wp_register_script(
			'sd-commission-calculator',
			plugin_dir_url( __FILE__ ). '/js/commission-calculator.min.js',
			null,
			filemtime( plugin_dir_path( __FILE__ )  . '/js/commission-calculator.min.js' ),
			true
		);

		wp_enqueue_script('sd-commission-calculator');

	}

	/**
	 * On plugin activation
	 */
	static function plugin_activated() {

		$activate = new activate_deactivate();
		$activate->activate();

	}

	/**
	 * On plugin deactivation we remove all the options
	 */
	static function plugin_deactivated() {

		$deactivate = new activate_deactivate();
		$deactivate->deactivate();

	}


	/**
	 * Stub method for updating the plugin
	 */
	static function plugin_updated() {

		/*
		 * For updates
		 */

	}

}

add_action( 'init', array( 'SolutionDesign\CommissionCalculator\commission_calculator', 'init' ) );
register_activation_hook( __FILE__, array( 'SolutionDesign\CommissionCalculator\commission_calculator', 'plugin_activated' ) );
register_deactivation_hook( __FILE__, array( 'SolutionDesign\CommissionCalculator\commission_calculator', 'plugin_deactivated' ) );