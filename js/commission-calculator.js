const commPriceInputs = document.querySelectorAll('.sdcc_range');
commPriceInputs.forEach(commPriceInput => commPriceInput.addEventListener('change', calculateComms));
let changeEvent = new Event('change', {bubbles: true});
commPriceInputs.forEach(commPriceInput => commPriceInput.dispatchEvent(changeEvent));

function calculateComms() {

    const formatter = new Intl.NumberFormat(document.documentElement.lang, {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });
    let commPercentage = this.dataset.comm / 100;
    let altCommPercentage = this.dataset.altComm / 100;
    let priceLabelId = document.querySelector('#' + this.dataset.priceLabelId);
    let priceId = document.querySelector('#' + this.dataset.priceId);
    let diffId = document.querySelector('#' + this.dataset.diffId);
    let displayCurrency = this.dataset.displayCurrency;
    let flatFee = this.dataset.flatFee;
    let roundTo = this.dataset.priceIncrements;
    let theCommission;
    let housePrice;

    if (roundTo > 0) {
        housePrice = Math.round(this.value / roundTo) * roundTo;
    } else {
        housePrice = formatter.format(this.value);
    }

    if (flatFee > 0) {
        theCommission = flatFee;
    } else {
        theCommission = housePrice * commPercentage;
    }

    let theAltCommission = housePrice * altCommPercentage;
    let theSaving = theAltCommission - theCommission;
    theSaving = Math.floor(theSaving);

    priceId.innerHTML = formatter.format(housePrice);

    if (this.dataset.showDiff === 'true') {
        diffId.innerHTML = formatter.format(theSaving);
    }
}