<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 11/15/2018
 * Time: 8:52 PM
 */

namespace SolutionDesign\CommissionCalculator;


class configuration {

	/*
	 * Default entries for the plugin options
	 */
	public $plugin_defaults = array(
		'lowest_price'         => '1',
		'highest_price'        => '100000000',
		'commission'           => '5',
		'competing_commission' => '8',
		'price_label'          => 'Your house price: ',
		'price_increments'     => 10000,
		'savings_label'        => 'Saved on commission: ',
		'title'                => '',
		'flat_fee'             => 0

	);

	/**
	 * Default styling classes used when generating shortcode
	 * //todo
	 *
	 * @var array
	 *
	 */
	private $default_image_class_list = array(
		'1' => 'commissionCalculatorWrapper',
		'2' => 'commissionCalculatorInput',
		'3' => 'commissionCalculatorTotal',
		'4' => 'commissionCalculatorCommission',
		'5' => 'commissionCalculatorSaving'
	);

	/*
	 * Holds plugin options
	 */
	public $plugin_settings;

	/*
	 * Default error message
	 */
	public $admin_error = 'There is a problem with your commission calculator settings.';

	public function __construct() {

		$this->get_settings();
	}

	/**
	 * Returns plugin settings
	 *
	 * @return array $plugin_settings
	 */
	public function get_settings() {

		$settings = get_option( 'sd_commission_calculator', array() );

		/*
		 * Set missing defaults
		 */
		foreach ( $this->plugin_defaults as $key => $value ) {

			if ( ! isset( $settings[ $key ] ) ) {
				$settings[ $key ] = $value;
			}

			if ( ! isset( $settings['show_difference'] ) ) {
				$settings['show_difference'] = 'false';
			}
		}

		$this->plugin_settings = $settings;

	}

}