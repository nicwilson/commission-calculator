<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/19/2018
 * Time: 4:30 PM
 */

namespace SolutionDesign\CommissionCalculator;


class shortcodes extends configuration {

	static $i = 1;

	public function __construct() {
		parent::__construct();
	}

	public function commission_calculator( $atts ) {

		self::$i ++;

		/* From shortcode attributes and settings or settings defaults */
		$title            = ( isset( $atts['title'] ) ) ? $atts['title'] : $this->plugin_settings['title'];
		$price_label      = ( isset( $atts['price_label'] ) ) ? $atts['price_label'] : $this->plugin_settings['price_label'];
		$price_increments = ( isset( $atts['price_increments'] ) ) ? $atts['price_increments'] : $this->plugin_settings['price_increments'];
		$savings_label    = ( isset( $atts['savings_label'] ) ) ? $atts['savings_label'] : $this->plugin_settings['savings_label'];
		$show_difference  = ( isset( $atts['show_difference'] ) ) ? 'true' : $this->plugin_settings['show_difference'];
		$min              = ( isset( $atts['lowest_price'] ) ) ? $atts['lowest_price'] : $this->plugin_settings['lowest_price'];
		$max              = ( isset( $atts['highest_price'] ) ) ? $atts['highest_price'] : $this->plugin_settings['highest_price'];
		$flat_fee         = ( isset( $atts['flat_fee'] ) ) ? $atts['flat_fee'] : $this->plugin_settings['flat_fee'];
		$value            = round( ( ( $max - $min ) / 2 ), -4 );

		/* Set unique IDs for elements */
		$input_id       = "sdcc-input-" . self::$i;
		$price_label_id = "sdcc-price-label-" . self::$i;
		$price_id       = "sdcc-price-" . self::$i;
		$diff_id        = "sdcc-diff-" . self::$i;

		/* Build html */
		ob_start();

		include( 'views/shortcode-commission-calculator.php' );

		$html = ob_get_clean();

		return $html;

	}
}