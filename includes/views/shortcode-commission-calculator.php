<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 12/21/2018
 * Time: 8:05 AM
 */ ?>

<div class="sdcc_container">

	<?php if ( ! empty( $title ) ): ?>
        <h3 class="sdcc_title"><?php echo $title; ?></h3>
	<?php endif; ?>

    <div class="sdcc_price_label" id="<?php echo $price_label_id; ?>">
		<?php echo $price_label; ?>
        <span class="sdcc_price" id="<?php echo $price_id; ?>"></span>
    </div>

    <input type="range"
           min="<?php echo $min; ?>"
           max="<?php echo $max; ?>"
           value="<?php echo $value; ?>"
           class="sdcc_range"
           id="<?php echo $input_id; ?>"
           data-price-label-id="<?php echo $price_label_id; ?>"
           data-price-id="<?php echo $price_id; ?>"
           data-price-increments="<?php echo $price_increments; ?>"
           data-diff-id="<?php echo $diff_id; ?>"
           data-show-diff="<?php echo $show_difference; ?>"
           data-comm="<?php echo $this->plugin_settings['commission']; ?>"
           data-alt-comm="<?php echo $this->plugin_settings['competing_commission']; ?>"
           data-flat-fee="<?php echo $flat_fee; ?>"
    />

	<?php if ( $show_difference === 'true' ): ?>

        <div class="sdcc_diff_label">
			<?php echo $savings_label; ?>
            <span class="sdcc_diff" id="<?php echo $diff_id; ?>"></span>
        </div>
	<?php endif; ?>

</div>