<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 11/16/2018
 * Time: 2:39 PM
 */

namespace SolutionDesign\CommissionCalculator;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class activate_deactivate {

	public function __construct() {
	}

	/**
	 * On activation stored version number and default settings
	 */
	public function activate() {

		$configuration = new configuration();
		$defaults      = $configuration->plugin_defaults;

		add_option( 'sd_commission_calculator_version', ( new commission_calculator() )->version, null, false );
		add_option( 'sd_commission_calculator', $defaults, null, false );

	}

	/**
	 * On deactivation remove version numbers and default settings
	 */
	public function deactivate() {

		delete_option( 'sd_commission_calculator_version' );
		delete_option( 'sd_commission_calculator' );

	}
}