<?php /** @noinspection SpellCheckingInspection */

/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 10/16/2018
 * Time: 4:23 PM
 */


namespace SolutionDesign\CommissionCalculator;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class plugin_setup extends configuration {


	protected static $instance = null;

	public static function init() {

		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/* Hook into the appropriate actions when the class is constructed. */
	public function __construct() {
		parent::__construct();

		$this->add_plugin_setup_page();

	}

	/*
	 * Add the plugin settings page to the settings menu
	 */
	public function add_plugin_setup_page() {

		add_submenu_page(
			'options-general.php',
			'Commission comparison calculator',
			'Commission calculator',
			'manage_options',
			'calculator-settings',
			array(
				$this,
				'render_setup_page'
			)
		);


		add_settings_section( 'calculator_settings', "Calculator settings", null, 'calculator-settings' );
		register_setting( 'calculator_settings', 'sd_commission_calculator' );

		$this->plugin_settings_fields();
	}

	/*
	 * Render the setup page
	 */
	public function render_setup_page() {

		ob_start();
		@include( 'views/render_setup_page.php' );
		$html = ob_get_clean();
		echo $html;

	}


	/*
	 * Create the plugin settings section fields and register them.
	 */
	public function plugin_settings_fields() {

		$fields = array();

		/**
		 *  Minimum house price field
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[lowest_price]',
				'title'        => 'Lowest price',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'text',
				'type'         => 'number',
				'min'          => 1,
				'max'          => 100000000,
				'style'        => 'width:80%;',
				'supplemental' => 'The lowest house price which may be entered in the slider ',
				'data'         => array( 'value' => $this->plugin_settings['lowest_price'] )
			)
		);

		/**
		 *  Maximum house price field
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[highest_price]',
				'title'        => 'Highest price',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'text',
				'type'         => 'number',
				'min'          => 1,
				'max'          => 100000000,
				'style'        => 'width:80%;',
				'supplemental' => 'The highest house price which may be entered in the slider ',
				'data'         => array( 'value' => $this->plugin_settings['highest_price'] )
			)
		);


		/**
		 * House price increments
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[price_increments]',
				'title'        => 'Increment house price by',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'select',
				'style'        => 'width:80%;',
				'supplemental' => 'What increment steps to apply to the house price amount',
				'data'         => $this->get_price_increment_field_html()
			)
		);


		/**
		 *   Flat fee commission
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[flat_fee]',
				'title'        => 'Flat fee commission',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'text',
				'type'         => 'number',
				'step'         => '0.01',
				'min'          => 0,
				'max'          => 10000000,
				'style'        => 'width:80%;',
				'supplemental' => 'If you enter a flat fee greater than 0, it will be used in the calculation rather than the commission percentage',
				'data'         => array( 'value' => $this->plugin_settings['flat_fee'] )
			)
		);


		/**
		 *   Commission
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[commission]',
				'title'        => 'Commission',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'text',
				'type'         => 'number',
				'step'         => '0.01',
				'min'          => 1,
				'max'          => 100000000,
				'style'        => 'width:80%;',
				'supplemental' => 'The commission you charge',
				'data'         => array( 'value' => $this->plugin_settings['commission'] )
			)
		);

		/*
		* Show the comparison
		*/
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[show_difference]',
				'title'        => 'Show comparison',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'input'        => 'tickbox',
				'section'      => 'calculator_settings',
				'supplemental' => 'Compare our commission to another commission',
				'data'         => array( 'value' => $this->plugin_settings['show_difference'] )
			)
		);

		/**
		 *   Commission
		 */
		array_push( $fields,
			array(
				'id'           => 'sd_commission_calculator[competing_commission]',
				'title'        => 'Competing commission',
				'callback'     => 'render_field',
				'page'         => 'calculator-settings',
				'section'      => 'calculator_settings',
				'input'        => 'text',
				'type'         => 'number',
				'step'         => '0.01',
				'min'          => 1,
				'max'          => 100000000,
				'style'        => 'width:80%;',
				'supplemental' => 'The commission you want to compare against',
				'data'         => array( 'value' => $this->plugin_settings['competing_commission'] )
			)
		);

		$this->register_fields( $fields );

	}

	private function get_price_increment_field_html() {

		$options = array( 0, 10, 100, 1000, 10000, 100000 );

		$html = '<select name="sd_commission_calculator[price_increments]">';

		foreach( $options as $option ){
			$selected = ( $this->plugin_settings['price_increments'] == $option ) ? " selected" : "";
			$html .= '<option value="' . $option . '"' . $selected . '>' . $option . '</option>';
		}

		$html .= '</select>';

		return $html;
	}

	/**
	 * Registers fields using array values in the $fields array
	 *
	 * @param $fields
	 *
	 * @return void
	 */
	public function register_fields( $fields ) {

		foreach ( $fields as $field ) {
			add_settings_field(
				$field['id'],
				$field['title'],
				array( $this, $field['callback'] ),
				$field['page'],
				$field['section'],
				$field
			);
		}

	}

	/**
	 * Echoes out the field views html based on the data in the $field array
	 *
	 * @param $field
	 */
	public function render_field( $field ) {

		$white_list = array( 'text', 'select', 'tickbox' );

		if ( ! in_array( $field['input'], $white_list ) ) {
			//return '';
		}

		ob_start();
		@include( 'views/render_' . $field['input'] . '_field.php' );
		$html = ob_get_clean();
		echo $html;

	}

	/**
	 * Renders the admin message
	 *
	 * @param string $message
	 */
	public function render_admin_message( $message = '' ) {

		ob_start();
		@include( 'views/render_admin_message.php' );
		$html = ob_get_clean();
		echo $html;

	}

}