<?php
/**
 * Created by PhpStorm.
 * User: nicdw
 * Date: 11/3/2018
 * Time: 3:20 PM
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

?>

<div class="wrap">

    <div id="icon-themes" class="icon32"></div>
    <h2>Commission calculator</h2>

    <form method="post" action="options.php">


		<?php settings_fields( 'calculator_settings' ); ?>
		<?php do_settings_sections( 'calculator-settings' ); ?>


		<?php submit_button(); ?>

    </form>

</div>
